# Maintainer: Knut Ahlers <knut at ahlers dot me>
# Contributor: Det <nimetonmaili g-mail> 
# Contributors: t3ddy, Lex Rivera aka x-demon, ruario

# Check for new Linux releases in: http://googlechromereleases.blogspot.com/search/label/Stable%20updates
# or use: $ curl -s https://dl.google.com/linux/chrome/rpm/stable/x86_64/repodata/other.xml.gz | gzip -df | awk -F\" '/pkgid/{ sub(".*-","",$4); print $4": "$10 }'

pkgname=google-chrome
pkgver=96.0.4664.45
pkgrel=1
pkgdesc="The popular and trusted web browser by Google (Stable Channel)"
arch=('x86_64')
url="https://www.google.com/chrome"
license=('custom:chrome')
options=('!emptydirs' '!strip')
install=${pkgname}.install
_channel=stable
source=("http://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-${_channel}/google-chrome-${_channel}_${pkgver}-1_amd64.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/google-chrome.appdata.xml"
        "eula_text.html::https://aur.archlinux.org/cgit/aur.git/plain/eula_text.html?h=google-chrome"
        "google-chrome-${_channel}.sh")
sha512sums=('98433b003d43627e221faad212cba3df42d7f3d6e31894b1e14a9058069cbcd2bd3c83b3c59ecc1733dab11e36b181fa1d89e33b841a21fd53a8e82bbddc39aa'
            'b216f2f2a83e7e1981260dda423e95e77ea30cf977505545aefbb576de737ddf060175d86556c6216f81fb1601b646b64f83c14e8d7ea496534267651c94004f'
            'a225555c06b7c32f9f2657004558e3f996c981481dbb0d3cd79b1d59fa3f05d591af88399422d3ab29d9446c103e98d567aeafe061d9550817ab6e7eb0498396'
            '956642602416b97fba999da29d64e2590eb21440b2b371913d3086676fba7235c7189c8d5994c3d2b80b1bf408feb75a72c69ff5aa35a08870bfc71c58886ecc')

package() {
    depends=('alsa-lib' 'gtk3' 'libcups' 'libxss' 'libxtst' 'nss' 'xdg-utils' 'ttf-liberation')
    optdepends=('kdialog: for file dialogs in KDE'
                'kwallet: for storing passwords in KWallet'
                'pipewire: WebRTC desktop sharing under Wayland'
                'org.freedesktop.secrets: password storage backend'
                'plasma-browser-integration: for integrate browsers into the Plasma Desktop')

    echo "  -> Extracting the data.tar.xz..."
    bsdtar -xf data.tar.xz -C "${pkgdir}/"

    echo "  -> Moving stuff in place..."
    # Launcher
    install -Dm755 google-chrome-${_channel}.sh "${pkgdir}"/usr/bin/google-chrome-${_channel}

    # Icons
    for i in 16x16 24x24 32x32 48x48 64x64 128x128 256x256; do
        install -Dm644 "${pkgdir}"/opt/google/chrome/product_logo_${i/x*}.png \
                    "${pkgdir}"/usr/share/icons/hicolor/$i/apps/google-chrome.png
    done

    # License
    install -Dm644 eula_text.html "${pkgdir}"/usr/share/licenses/google-chrome/eula_text.html
    install -Dm644 "$pkgdir"/opt/google/chrome/WidevineCdm/LICENSE \
        "$pkgdir"/usr/share/licenses/google-chrome-$_channel/WidevineCdm-LICENSE.txt

    echo "  -> Fixing Chrome icon resolution..."
    sed -i \
        -e "/Exec=/i\StartupWMClass=Google-chrome" \
        -e "s/x-scheme-handler\/ftp;\?//g" \
            "${pkgdir}"/usr/share/applications/google-chrome.desktop

    echo "  -> Removing Debian Cron job, duplicate product logos and menu directory..."
    rm -r \
        "$pkgdir"/etc/cron.daily/ \
        "$pkgdir"/opt/google/chrome/cron/ \
        "$pkgdir"/opt/google/chrome/product_logo_*.{png,xpm} \
        "$pkgdir"/usr/share/menu/

    # Appstream
    rm -rf "${pkgdir}/usr/share/appdata"
    install -Dm644 "${srcdir}/google-chrome.appdata.xml" "${pkgdir}/usr/share/metainfo/google-chrome.appdata.xml"
}
